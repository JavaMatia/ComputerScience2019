package Day2;

public class File
{
    private String name; // file name
    private String fileExtenstion; //file externsion
    private double size; //the size of the file
    private String creationDate; //the creation date of the file
    private boolean isOpen; // is the file open?
    private String contents; //the content of the file

    public File(String name, String fileExtenstion, double size, String creationDate, boolean isOpen, String contents)
    {
        this.name = name;
        this.fileExtenstion =  fileExtenstion;
        this.size = size;
        this.creationDate = creationDate;
        this.isOpen = isOpen;
        this.contents = contents;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFileExtenstion()
    {
        return fileExtenstion;
    }

    public void setFileExtenstion(String fileExtenstion)
    {
        this.fileExtenstion = fileExtenstion;
    }

    public double getSize()
    {
        return size;
    }

    public void setSize(double size)
    {
        this.size = size;
    }

    public String getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(String creationDate)
    {
        this.creationDate = creationDate;
    }

    public boolean isOpen()
    {
        return isOpen;
    }

    public void setOpen(boolean open)
    {
        isOpen = open;
    }

    public String getContents()
    {
        return contents;
    }

    public void setContents(String contents)
    {
        this.contents = contents;
    }

    public String toString()
    {
        return "File{" +
                "Name='" + name + '\'' +
                ", File Extenstion='" + fileExtenstion + '\'' +
                ", Size=" + size +
                ", creation Date='" + creationDate + '\'' +
                ", Is the file open?=" + isOpen +
                ", Contents='" + contents + '\'' +
                '}';
    }
    public boolean sameType(File file)
    {
        return this.fileExtenstion.equals(file.fileExtenstion);
    }
}

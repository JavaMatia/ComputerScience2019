package Day2;
import java.util.Scanner;

public class FileMain
{
    private static Scanner in = new Scanner(System.in);
    public static void main(String[] args)
    {
        String name; // file name
        String fileExtenstion;
        double size;
        String creationDate;
        boolean isOpen;
        String contents;
        System.out.println("Enter details for file 1: ");

        name = in.nextLine();
        fileExtenstion = in.nextLine();
        size = in.nextDouble();
        in.nextLine(); //clean \n
        creationDate = in.nextLine();
        isOpen = in.nextBoolean();
        in.nextLine(); //clean \n
        contents = in.nextLine();
        File file1 = new File(name, fileExtenstion, size, creationDate, isOpen, contents);

        System.out.println("Enter details for file 2: ");
        name = in.nextLine();
        fileExtenstion = in.nextLine();
        size = in.nextDouble();
        in.nextLine(); //clean \n
        creationDate = in.nextLine();
        isOpen = in.nextBoolean();
        in.nextLine(); //clean \n
        contents = in.nextLine();

        File file2 = new File(name, fileExtenstion, size, creationDate, isOpen, contents);

        if (file1.sameType(file2))
        {// the files are the same type
            file1.setOpen(true);
            file2.setOpen(true);
            name = in.nextLine();
            creationDate = in.nextLine();
            File file3 = new File(name, file1.getFileExtenstion(),
                    file1.getSize() + file2.getSize(), creationDate, true,
                    file1.getContents() + file2.getContents());
            System.out.println(file3.toString()); // print the file
            file3.setOpen(false);
        }
        else{ System.out.println("Files do not match!"); }
        file1.setOpen(false);
        file2.setOpen(false);
    }
}

package Day2;

import java.util.Scanner;

public class Polygon
{
    public static Scanner in = new Scanner(System.in);

    private int numOfPoints; // number of points
    private Point[] points = new Point[20]; // array of points

    public int getNumOfPoints()
    {
        return numOfPoints;
    }

    public Polygon(int numOfPoints)
    {
        this.numOfPoints = numOfPoints;
        for (int i = 0; i<this.numOfPoints; i++)
        {
            System.out.println("Enter coordinate (x, y) for point " + (i+1));
            points[i] = new Point(in.nextDouble(), in.nextDouble());
        }
    }
    public void addPoint(Point p)
    {
        if (this.numOfPoints < 20)
        {
            //we can add a point
            points[numOfPoints] = p;
            numOfPoints++; // increment the number of points
        }
    }
    public double getPerimeter()
    {
        double perimeter = 0;
        Point firstPoint = points[0];
        for (int i = 0; i<this.numOfPoints-1; i++)
        {
            perimeter += points[i].calculateDistance(this.points[i+1]);
        }
        // calculate distance between the last and first points
        perimeter += points[numOfPoints-1].calculateDistance(firstPoint);
        return perimeter;
    }
    public double shortestLine()
    {
        double shortest = this.points[0].calculateDistance(this.points[1]);
        for (int i = 1; i<this.numOfPoints-1; i++)
        {
            if (points[i].calculateDistance(points[i + 1]) < shortest)
            {
                shortest = points[i].calculateDistance(points[i + 1]);
            }
        }
        // calculate distance between the last and first points
        if (points[numOfPoints-1].calculateDistance(points[0]) < shortest)
        {
            shortest = points[numOfPoints-1].calculateDistance(points[0]);
        }
        return shortest;
    }


    public String toString()
    {
        String description = "There are " + this.numOfPoints + " points in this polygon. The points are: \n";
        for (int i = 0; i<numOfPoints; i++)
        {
           description = description.concat(i+1+". " + points[i].toString())+"\n";
        }

        return description ;
    }
}

package Day2;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int numOfPoints;
        System.out.println("Enter the number of points your polygon has: ");
        numOfPoints = in.nextInt();

        Polygon polygon = new Polygon(numOfPoints); // create new polygon
        System.out.println("Shortest side of the polygon: " + polygon.shortestLine());
        System.out.println("The perimeter of the polygon: "+ polygon.getPerimeter());
        System.out.println(polygon.toString());
        //create new point
        System.out.println("Enter coordinates: ");
        Point newPoint = new Point(in.nextDouble(), in.nextDouble());
        polygon.addPoint(newPoint);
        System.out.println(polygon.toString());
    }
}

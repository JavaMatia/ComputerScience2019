package Recurtion;

public class HW12_10_18
{
    private static int sumNumber(int number)
    {
        if (number < 10)
        {
            return number;
        }
        return sumNumber(number / 10) + number % 10;
    }

    private static boolean inNumber(int number, int digit)
    {
        if (number < 10)
        {
            return false;
        }
        if (number % 10 == digit)
        {
            return true;
        }
        return inNumber(number / 10, digit);
    }

    private static void displayNewLine(String str)
    {
        if (str.length() > 0)
        {
            System.out.println(str.charAt(0));
            displayNewLine(str.substring(1));

        }
    }

    private static void displayNewLine2(String str)
    {
        if (str.length() > 0)
        {
            System.out.println(str.charAt(str.length() - 1));
            displayNewLine2(str.substring(0, str.length() - 1)); // substring end index doesn't count

        }
    }

    public static void main(String[] args)
    {
        System.out.println(sumNumber(1234));
        System.out.println(inNumber(123, 4));
        System.out.println();
        displayNewLine2("Elad");
    }
}

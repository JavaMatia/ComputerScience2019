package Recurtion;

public class RecursiveHW
{
    public static int multiplication(int x, int y)
    {
        if (y == 1)
        {
            return x;
        }
        return multiplication(x, y-1) + x;
    }
    public static void pow2(int n)
    {
        if (n > 0)
        {
            pow2(n-1);
        }
        System.out.print( (int) Math.pow(2, n) + ", ");

    }
    public static void pow3(int n)
    {
        if (n>0)
        {
            System.out.print(n+", ");
            pow3(n/2);
        }
    }

    public static void main(String[] args)
    {
        System.out.println(multiplication(7, 6));

        pow2(16);
        System.out.println();
        pow3(16);
    }
}
